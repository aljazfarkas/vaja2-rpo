﻿using System;

namespace Vaja2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Seznam na začetku:");
            int[] seznam = { 2, 7, 3, 7, 1, 8, 3 };
            for (int i = 0; i < seznam.Length; i++)
            {
                Console.Write(seznam[i] + " ");
            }
            ShakerSort(seznam);
            Console.WriteLine("Seznam na koncu:");
            for (int i = 0; i < seznam.Length; i++)
            {
                Console.Write(seznam[i] + " ");
            }
        }
        public static void ShakerSort(int[] array)
        {
            for (int i = 0; i < array.Length / 2; i++)
            {
                bool swapped = false;
                for (int j = i; j < array.Length - i - 1; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        int tmp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = tmp;
                        swapped = true;
                    }
                }
                for (int j = array.Length - 2 - i; j > i; j--)
                {
                    if (array[j] > array[j - 1])
                    {
                        int tmp = array[j];
                        array[j] = array[j - 1];
                        array[j - 1] = tmp;
                        swapped = true;
                    }
                }
                if (!swapped) break;
            }
        }
    }
}
